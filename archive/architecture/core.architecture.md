+++
title = "Core Architecture"
weight = 1
parent = "Architecture"
+++

{{% notice info %}}

This is the _reference architecture_ for an Xalgorithms _implementation_. There
may be many different implementations of this architecture. They will depend on
technical context and the specific needs of the implementors. This document
serves to illustrate the basic components and behaviours of _any_ of those
implementations.

{{% /notice %}}

Once upon a time, in Canada, there was a store called Consumer's
Distributing. It was an incredibly simple place. There were several kiosks with
product catalogs on them and a few queues for taking customer orders. To make a
purchase at this store, you browsed the catalog to find what it was that you
wanted to buy. You wrote down the SKUs of each item on a specially prepared
order sheet. When you finished writing everything you wanted onto the paper, you
took it to one of the queues where a cashier would go into a warehouse in the
back of the store to pick your orders. After a few minutes the clerk would
return with your product or tell you that they did not have it in stock.

This story is an illustrative example of the way that the Xalgorithms is
designed. Quite simply: Xalgorithms is a "store" where "rules" are ordered from
a catalog and given to the "customer" for their use.

Throughout this document we will refer back to this analogy and we will use the
various elements from it to illustrate how the solution would function.

# Breaking down the analogy

## Artifacts

There are some atomic elements in the Xalgorithms architecture that need
explaining. If you consider the architecture a story, consider these the nouns.

### The Product {#product}

The core "product" of this solution is called a _rule_. A rule is truth table
that can be evaluated in the context of a "document" provided by an integrated
solution. A document is a hierarchical collection of key-value pairs represented
in JSON format. Using this format, integrated solutions can properly represent
the context in which a rule should be evaluated.

Rules also include meta-information that determines [whether the rule applies in
particular contexts]({{<ref "/concepts/matching" >}}).

For example: The order sheet in our analogy could be represented by a JSON array
in a document:


```
{
  "orders" : [
    { "SKU": "AF12345", "description" : "Light bulb", "price" : 6.95 },
    { "SKU": "ZZ54321", "description" : "Toaster oven", "price" : "149" }
  ]
}
```

Evaluating this document in the context of our analogy, there are a number of
immediately obvious rules that might be applied to it. For one, there's no sales
tax in the order document, a rule could be written to determine the correct
taxes based on the `price` key. That rule could even include tax exceptions
based on the type of product.

### The Customer {#customer}

The "customer" in this solution is another software solution that has the goal
of standardizing its behaviour against a common set of shared rules. These rules
could apply to many different domains - this solution is domain-agnostic and
does not constrain itself to any one in particular.

An example of another software solution could be an application that manages a
set of Consumer's Distributing stores. We will visit this in more detail later
on when discussing [an example deployment]({{<ref "#example_deployment">}}).

### The Warehouse {#warehouse}

Behind every store is a warehouse stocked with the products that appear in the
catalog. Each one of them has a unique identifier that allows a clerk to quickly
locate the product for the customer. If you look behind the scenes this is not a
single warehouse - it is a supply chain of interconnected warehouses. We might
imagine that in each region where there are stores, there are regional warehouses
that stock products that sell well in that region. Each store warehouse might
itself be stocked with products that sell well in that store. Between these
warehouses is a logistics network that refines and maintains a steady supply of
in-demand products.

Similarly, in this solution, we have a "warehouse" of "products". This
"warehouse" is called a _rule reserve_. This is a versioned data store that
contains files that follow a standard schema. Each file is a single rule. The
reserve serves to organize the rules, and act as a network service that supplies
the rules when they need to be evaluated.

There is no single rule reserve. There is a network of reserves where each
individual reserve is simply a peer.

Producers and consumers of rules might each talk to more that one reserve in
order to retrieve rules for multiple domains (or some other partitioning
factor). There may be _authoritative reserves_ that contain a large volume of
rules where competing consumers or producers can trust that the contained rules
follow agreed upon standards. There may be _downstream reserves_ that filter the
rule sets from larger reserves in order to satisfy a particular application.

### The Catalog {#catalog}

In order for a customer to efficiently locate the products they want to buy, the
store provides a catalog. The catalog contains broad categories of products and
an index for quick reference. Similarly, there is a catalog of rules called a
_rulelog_.

The rulelog stores meta-information about rules, but not the rules themselves,
The rulelog includes a unique reference that can be used to retrieve each
rule. This meta-information is optimized to make applying the [in-effect and
applicable]({{<ref "/concepts/matching" >}}) filtering as performant as
possible.

Since there can be multiple rule reserves, the catalog is an aggregation of the
rules in one or more reserves. This might be a complete aggregation where all of
the rules in all associated reserves are represented or it might be a subset of
the rule where the aggregation has been filtered down to a particular concern.

The catalog is a _single data file_ that can be easily transmitted across a
network.

## Major Roles

Where the artifacts where the nouns, the _roles_ are the verbs. They are active
actors that cause information and control to move throughout the solution. The
activity of each role is broken down into multiple stages.

These stages allow for variance in implementations and deployments of this
architecture. We have designed the solution to allow both us and other
implementors to author different modules and services that act in these roles
and their stages.

### Production of Rules {#production}

All of the products in the store must be produced in a factory. Similarly,
_rules_ are produced by _rule authors_.

The XA Rule Maker (XA-RM) is used by rule authors to create or update rules. It
is capable of communicating with one or more reserves. We expect that XA-RM
would be a complete application or, at least, a broad set of recommended
application components that could be assembled into a working application.

#### Describe

#### Maintain

#### Compile

### Distribution of Rules {#distribution}

As [previously mentioned]({{<ref "#catalog">}}), we will need to provide an
aggregate catalog spanning rule reserves that concern a particular integration
application.

#### Disseminate

### Application of Rules {#application}

In the actual store, customers browse the catalog and select the products that
they would like to buy. Similarly, an integrated application will need to
filter, select and apply rules that are appropriate for its domain. This
capability is provided by a component called XA Rule Taker (XA-RT). We do not
anticipate that XA-RT would be a _complete application_ in its own right, merely
a reusable library or module.

Any implementation of XA-RT should be able to process _any_ information in the
catalog and apply the [protocol]({{<ref "/concepts/matching" >}}).

#### Accomplish

#### Evaluate

# Deployment Constellations {#constellations}

# An Example Implementation {#implementation}
